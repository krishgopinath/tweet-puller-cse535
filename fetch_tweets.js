 var Twitter = require('twitter');
 var fs = require('fs');
 var qs = require('querystring');

 // #PrayForParis fr 2015-11-25

 function exit(message) {
   console.log(message);
   process.exit(-1);
 }

 var date = new Date();

 var lang = process.argv[3];
 var since = process.argv[4];
 var until = process.argv[5];

 var q = process.argv[2] + (since ? 'since:' + since : '') + (until ? 'until:' + until : '');

 var count = 100;

 var maxPerDate = 3;
 var params = {};

 var client = new Twitter({
   consumer_key: 'CJ2sBg2N9vR3mBTxUwj4A5nOV',
   consumer_secret: '9HkO0DWUXbJxJDjPzHUbGSCvcPixbcRw9lIAoXvWEYXs1LktvS',
   access_token_key: '114136828-cIJI32nCjqBwxlrSMLJ5ffHXS5uMcWOIds6Uw6Xv',
   access_token_secret: 'BpdITUHhXYTIveFwKO2UgPR2WGGT69SNKN1fDN8vALOOU'
 });

 (function getTweets(runCount) {
   console.log(runCount);
   if (runCount >= maxPerDate) {
     exit("This is the max i can do in one round. retry if you want");
   } else {
     var dir = './tweets-' + lang + '/'; // your directory

     if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
     }

     var latestFile = fs.readdirSync(dir).filter(function(v) {
       if (v.split('_')[0] == q) {
         return v;
       }
     }).map(function(v) {
       return {
         name: v,
         time: fs.statSync(dir + v).mtime.getTime()
       }
     }).sort(function(a, b) {
       return b.time - a.time;
     }).map(function(v) {
       return v.name;
     })[0];

     if (latestFile) {
       //read contents of json file
       var tweetSet = JSON.parse(fs.readFileSync(dir + latestFile, 'utf-8'));
       //get max_id of that
       if (tweetSet.search_metadata.next_results) {
         var qsParams = tweetSet.search_metadata.next_results.replace('?', '');
         //add to params
         params = qs.parse(qsParams);
       } else {
         exit('there is no more data here. try some other query.');
       }

     } else {
       params = {
         q: q,
         lang: lang,
         count: count
       }
     }

     console.log(params);

     client.get('search/tweets', params, function(error, tweets, response) {
       if (!error) {
         fs.writeFileSync(dir + params.q + '_' + lang + '_' + Math.random() + '.json', JSON.stringify(tweets, null, 4), 'utf-8');
         getTweets(++runCount);
       }
     });

   }
 })(0);
