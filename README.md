Disclaimer
---

This project is a year old. I didn't test it. It worked a year ago, it should most _prooobbbably_ work now. Apologies if this doesn't work on the first try!


Instructions to use this
---

As always, `clone` the repo and `cd` into the directory: 


    cd tweet-puller-cse565


Make sure you have `node` is installed and verify if `node` and `npm` are available in `$PATH`. And then, run this at the root of the directory

    tweet-puller-cse565> npm install

[Edit the API credentials in the code, starting here.](https://gitlab.com/krishgopinath/tweet-puller-cse535/blob/master/fetch_tweets.js#L25)

     var client = new Twitter({
      consumer_key: 'Your stuff here',
      consumer_secret: 'Your stuff here',
      access_token_key: 'Your stuff here',
      access_token_secret: 'Your stuff here'
    });

Once that is done, run 

    node fetch_tweets.js "Query here" --lang --since --until

For example, for the query **#PrayForParis** with language, since and until dates, 

    node fetch_tweets.js "#PrayForParis" "fr" "2015-11-25" "2015-11-27"

* The quotes around the params might or might not be necessary. Trial and error.
* The `--since` and `--until` options are optional!
* [For the list of languages that could be used, check this out!](https://dev.twitter.com/web/overview/languages)